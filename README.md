# Welcome to The Expert Box Community

This is the place where friends, evangelists, supporters, campaigners, enthusiasts, and all other users of The Expert Box can interact with us in a public forum.

While you are always welcome to [contact us](https://www.ExpertBox.com/contact) in private, here you can do so in public.

Please note that we recently migrated from GitHub to GitLab and are re-organising all our issues and roadmap, which we will soon start publishing here.